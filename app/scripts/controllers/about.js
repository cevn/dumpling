'use strict';

angular.module('dumplingApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.message = 'dumpling is a new webapp designed to help users learn about what they eat!';
  });
